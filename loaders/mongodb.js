const mongoose = require("mongoose");

module.exports = async (cb) => {
  let uri = `${process.env.MONGO_USER}`
    ? `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASS}@${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`
    : `mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`;

  console.info(`connectionString : ${uri}`);

  mongoose.connect(uri, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true,
    useUnifiedTopology: true,
  });

  mongoose.connection.on("connected", (err) => {
    if (err) {
      console.error(err);
    } else {
      console.info(`✓ MongoDB Connected`);
      return cb ? cb() : null;
    }
  });

  mongoose.connection.on("disconnected", (err) => {
    console.info(`✓ MongoDB Disconnected : ${err}`);
    mongoose.connect(uri);
  });

  mongoose.connection.on("error", (err) => {
    console.error(
      `✗ MongoDB connection error. Please make sure MongoDB is running. : ${err}`
    );
    process.exit();
  });
};
