const mongoose = require("mongoose");

const docSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      index: true,
    },
    bookmarkUserId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      index: true,
    },
    isLocked: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

docSchema.indexes();

const bookmarkUser = mongoose.model("bookmarkUser", docSchema);

module.exports = bookmarkUser;
