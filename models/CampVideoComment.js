const mongoose = require("mongoose");
const logger = require("../utils/logger");

const docSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      index: true,
    },
    campVideoId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "campaignVideo",
      index: true,
    },
    message: {
      type: String,
    },
  },
  { timestamps: true }
);

docSchema.indexes();

const campVideoComment = mongoose.model("campVideoComment", docSchema);

campVideoComment.once("index", (err) =>
  err ? logger.warn("Campaign Video comment Models index error : ", err) : ""
);

module.exports = campVideoComment;
