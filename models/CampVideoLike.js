const mongoose = require("mongoose");
const logger = require("../utils/logger");

const docSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      index: true,
    },
    campVideoId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "campaignVideo",
      index: true,
    },
  },
  { timestamps: true }
);

docSchema.index({ userId: 1, campVideoId: 1 }, { unique: true });
docSchema.indexes();

const campVideoLike = mongoose.model("campVideoLike", docSchema);

campVideoLike.once("index", (err) =>
  err ? logger.warn("Campaign Video like Models index error : ", err) : ""
);

module.exports = campVideoLike;
