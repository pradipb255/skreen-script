const mongoose = require("mongoose");
const logger = require("../utils/logger");

const docSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      index: true,
    },
    campVideoId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "campaignVideo",
      index: true,
    },
    count: {
      type: Number,
      default: 1,
    },
  },
  { timestamps: true }
);

docSchema.index({ userId: 1, campVideoId: 1 }, { unique: true });
docSchema.indexes();

const campVideoView = mongoose.model("campVideoView", docSchema);

campVideoView.once("index", (err) =>
  err ? logger.warn("Campaign Video view Models index error : ", err) : ""
);

module.exports = campVideoView;
