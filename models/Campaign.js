const mongoose = require("mongoose");
const moment = require("moment");
const User = require("./User");

const docSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      index: true,
    },
    title: {
      type: String,
    },
    isActive: {
      type: Boolean,
      default: true,
    },
    expiryDate: {
      type: Date,
      set: (v) =>
        moment(v).set({ hour: 0, minute: 0, second: 0, millisecond: 0 }),
    },
    instruction: {
      type: String,
    },
    location: {
      type: {
        type: String,
        enum: ["Point"],
      },
      coordinates: [
        {
          type: Number,
        },
      ],
    },
    address: {
      type: String,
    },
    interest: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "interest",
      },
    ],
    maxApplicants: {
      type: Number,
      default: 100,
    },
    availApplicants: {
      type: Number,
      default: 0,
    },
  },
  { timestamps: true }
);

docSchema.index({
  createdAt: 1,
});
docSchema.index({
  location: "2dsphere",
});
docSchema.indexes();

/**
 * campaign in active middleware.
 */
docSchema.pre("save", function save(next) {
  const campaign = this;

  if (!(campaign.isModified("isActive") && campaign.isActive === false)) {
    return next();
  }

  User.updateOne(
    {
      _id: campaign.userId,
    },
    {
      $inc: {
        campaignCount: -1,
      },
    }
  )
    .then(next)
    .catch(next);
});

const campaign = mongoose.model("campaign", docSchema);

module.exports = campaign;
