const mongoose = require("mongoose");

const docSchema = new mongoose.Schema(
  {
    campId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "campaign",
      index: true,
    },
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      index: true,
    },
    notes: {
      type: String,
      default: "",
    },
    video: {
      type: String,
    },
    videoThumb: {
      type: String,
    },
    videoType: {
      type: String,
      enum: ["userVideo", "campaignSubmission"],
    },
    isRead: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

const campaignApplicant = mongoose.model("campaignApplicant", docSchema);

module.exports = campaignApplicant;
