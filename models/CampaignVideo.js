const mongoose = require("mongoose");
const { s3 } = require("./../config");

let setOldDoc = function (that, field) {
  if (!that._oldDoc) that._oldDoc = {};
  that._oldDoc[field] = that._doc[field];
};

const docSchema = new mongoose.Schema(
  {
    campaignId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "campaign",
      index: true,
    },
    video: {
      type: String,
      get: function (value) {
        if (!isNaN(value)) return "";
        return `${s3.baseURL}${s3.path.campaignVideo.base}${value}`;
      },
      set: function (newVal) {
        setOldDoc(this, "video");
        return newVal;
      },
    },
    videoThumb: {
      type: String,
      get: function (value) {
        if (!isNaN(value)) return "";
        return `${s3.baseURL}${s3.path.campaignVideo.base}${value}`;
      },
      set: function (newVal) {
        setOldDoc(this, "videoThumb");
        return newVal;
      },
    },
    isEnabled: {
      type: Boolean,
      default: true,
    },
    commentCount: {
      type: Number,
      default: 0,
    },
    likeCount: {
      type: Number,
      default: 0,
    },
    viewCount: {
      type: Number,
      default: 0,
    },
  },
  { timestamps: true }
);

const campaignVideo = mongoose.model("campaignVideo", docSchema);

module.exports = campaignVideo;
