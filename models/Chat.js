const mongoose = require("mongoose");
const logger = require("../utils/logger");

const docSchema = new mongoose.Schema(
  {
    roomId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "room",
      required: true,
      index: true,
    },
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      required: true,
      index: true,
    },
    message: {
      type: String,
    },
  },
  { timestamps: true }
);

docSchema.index({
  createdAt: 1,
});
docSchema.indexes();

const chat = mongoose.model("chat", docSchema);

chat.once("index", (err) =>
  err ? logger.warn("Chat Models index error : ", err) : ""
);

module.exports = chat;
