const mongoose = require("mongoose");
const logger = require("../utils/logger");
const Notification = require("./Notification");

const docSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      index: true,
    },
    followUserId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      index: true,
    },
    type: {
      type: String,
      enum: ["jobSeeker", "talentSeeker"],
    },
  },
  { timestamps: true }
);

docSchema.indexes();

// Only document middleware
docSchema.pre("remove", { document: true }, function (next) {
  Notification.deleteMany({
    activityType: "following",
    objectId: this._id,
  })
    .then(() => next())
    .catch(next);
});

// Only query middleware. This will get called when you do `Model.remove()`
// but not `doc.remove()`.
docSchema.pre("remove", { query: true }, function (next) {
  Notification.deleteMany({
    activityType: "following",
    objectId: this._id,
  })
    .then(() => next())
    .catch(next);
});

const follow = mongoose.model("follow", docSchema);

follow.once("index", (err) =>
  err ? logger.warn("Follow Models index error : ", err) : ""
);

module.exports = follow;
