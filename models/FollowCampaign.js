const mongoose = require("mongoose");
const logger = require("../utils/logger");

const docSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      index: true,
    },
    campId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "campaign",
      index: true,
    },
  },
  { timestamps: true }
);

docSchema.indexes();

const followCampaign = mongoose.model("followCampaign", docSchema);

followCampaign.once("index", (err) =>
  err ? logger.warn("Campaign Follow Models index error : ", err) : ""
);

module.exports = followCampaign;
