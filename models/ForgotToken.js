const mongoose = require("mongoose");
const logger = require("../utils/logger");
const moment = require("moment");

const docSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      index: true,
    },
    token: {
      type: String,
      unique: true,
    },
    isActive: {
      type: Boolean,
      default: true,
    },
  },
  { timestamps: true }
);

docSchema.index({ createdAt: 1 }, { expireAfterSeconds: 86400 }); // delete document after 30 seconds

docSchema.statics.generateCode = function getNumber() {
  return new Promise((resolve, reject) => {
    var n = Math.floor(10000 + Math.random() * 90000);
    this.findOne({ token: n }, function (err, result) {
      if (err) return reject(err);
      else if (result) return getNumber();
      else return resolve(n);
    });
  });
};

docSchema.statics.verifyToken = function (token) {
  return new Promise((resolve, reject) => {
    this.findOne({
      token,
      isActive: true,
      createdAt: {
        $gt: moment().subtract("5", "minutes").toDate(),
      },
    })
      .then(resolve)
      .catch(reject);
  });
};

docSchema.statics.expireToken = function (token) {
  return new Promise((resolve, reject) => {
    this.findOne({
      token: token,
    })
      .then(async (token) => {
        if (token) {
          token.isActive = false;
          await token.save();
        }
        return resolve();
      })
      .catch(reject);
  });
};

docSchema.statics.findLatestToken = function (email) {
  return new Promise((resolve, reject) => {
    this.findOne({
      email,
      isActive: true,
    })
      .sort({
        _id: -1,
      })
      .then(resolve)
      .catch(reject);
  });
};

const forgotToken = mongoose.model("forgotToken", docSchema);

forgotToken.once("index", (err) =>
  err ? logger.warn("Forgot token Models index error : ", err) : ""
);

module.exports = forgotToken;
