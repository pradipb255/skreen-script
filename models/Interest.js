const mongoose = require("mongoose");

const docSchema = new mongoose.Schema(
  {
    name: {
      type: String,
    },
  },
  { timestamps: true }
);

const interest = mongoose.model("interest", docSchema);

module.exports = interest;
