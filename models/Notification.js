const mongoose = require("mongoose");
const logger = require("../utils/logger");

const docSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      index: true,
    },
    activityType: {
      type: String,
      enum: [
        "videoComment",
        "campVideoComment",
        "videoLike",
        "campVideoLike",
        "following",
        "followingCampaign",
        "userVideo",
        "shareProfile",
      ],
    },
    objectType: {
      type: String,
      enum: [
        "videoComment",
        "campVideoComment",
        "videoLike",
        "campVideoLike",
        "follow",
        "followCampaign",
        "userVideo",
        "shareProfile",
      ],
    },
    objectId: {
      type: mongoose.Schema.Types.ObjectId,
      refPath: "objectType",
    },
    desc: {
      type: String,
      default: "",
    },
    status: {
      type: String,
      enum: ["pending", "viewed"],
      index: true,
      default: "pending",
    },
  },
  {
    timestamps: true,
  }
);

docSchema.indexes();

// static method for populate notification attributes
docSchema.statics.populateDocs = async function (docs) {
  for (let doc of docs) {
    switch (doc.activityType) {
      case "videoComment":
      case "videoLike":
        await doc
          .populate({
            path: "objectId",
            select: "userId userVideoId",
          })
          .execPopulate();
        await doc
          .populate({
            path: "objectId.userId",
            select: "firstName lastName profilePic",
          })
          .execPopulate();
        await doc
          .populate({
            path: "objectId.userVideoId",
            select: "video videoThumb",
          })
          .execPopulate();
        break;
      case "campVideoComment":
      case "campVideoLike":
        await doc
          .populate({
            path: "objectId",
            select: "userId campVideoId",
          })
          .execPopulate();
        await doc
          .populate({
            path: "objectId.userId",
            select: "firstName lastName profilePic",
          })
          .execPopulate();
        await doc
          .populate({
            path: "objectId.campVideoId",
            select: "campaignId video videoThumb",
          })
          .execPopulate();
        break;
      case "following":
        await doc
          .populate({
            path: "objectId",
            select: "userId",
          })
          .execPopulate();
        await doc
          .populate({
            path: "objectId.userId",
            select: "firstName lastName profilePic",
          })
          .execPopulate();
        break;
      case "followingCampaign":
        await doc
          .populate({
            path: "objectId",
            select: "userId campId",
          })
          .execPopulate();
        await doc
          .populate({
            path: "objectId.userId",
            select: "firstName lastName profilePic",
          })
          .execPopulate();
        await doc
          .populate({
            path: "objectId.campId",
            select: "_id",
          })
          .execPopulate();
        break;
      case "userVideo":
        await doc
          .populate({
            path: "objectId",
            select: "userId video videoThumb",
          })
          .execPopulate();
        await doc
          .populate({
            path: "objectId.userId",
            select: "firstName lastName profilePic",
          })
          .execPopulate();
        break;
      case "shareProfile":
        await doc
          .populate({
            path: "objectId",
            select: "userId videoId sourceId sourceType",
          })
          .execPopulate();
        await doc
          .populate({
            path: "objectId.userId",
            select: "firstName lastName profilePic",
          })
          .execPopulate();
        switch (doc.objectId.sourceType) {
          case "user":
            await doc
              .populate({
                path: "objectId.sourceId",
                model: "user",
                select: "profilePic",
              })
              .execPopulate();
            break;
          case "campaign":
            await doc
              .populate({
                path: "objectId.sourceId",
                model: "campaign",
                select: "userId",
              })
              .execPopulate();
            await doc
              .populate({
                path: "objectId.sourceId.userId",
                model: "user",
                select: "profilePic",
              })
              .execPopulate();
            break;
        }
        break;
      default:
        break;
    }
  }
  return attachMessage(docs);
};

let attachMessage = (docs) => {
  for (let i = 0; i < docs.length; i++) {
    docs[i] = docs[i].toObject();
    switch (docs[i].activityType) {
      case "videoComment":
        docs[i].user = docs[i].objectId.userId;
        docs[i].userVideo = docs[i].objectId.userVideoId;
        docs[i].message = "Commented on your video";
        break;
      case "campVideoComment":
        docs[i].user = docs[i].objectId.userId;
        docs[i].campaignVideo = docs[i].objectId.campVideoId;
        docs[i].message = "Commented on your video";
        break;
      case "videoLike":
        docs[i].user = docs[i].objectId.userId;
        docs[i].userVideo = docs[i].objectId.userVideoId;
        docs[i].message = "Liked your video";
        break;
      case "campVideoLike":
        docs[i].user = docs[i].objectId.userId;
        docs[i].campaignVideo = docs[i].objectId.campVideoId;
        docs[i].message = "Liked your video";
        break;
      case "following":
        docs[i].user = docs[i].objectId.userId;
        docs[i].message = "Started following you";
        break;
      case "followingCampaign":
        docs[i].user = docs[i].objectId.userId;
        docs[i].campaignId = docs[i].objectId.campId._id;
        docs[i].message = "Started following your campaign";
        break;
      case "userVideo":
        docs[i].user = docs[i].objectId.userId;
        docs[i].userVideo = docs[i].objectId;
        delete docs[i].objectId.userId;
        docs[i].message = "Posted a new video";
        break;
      case "shareProfile":
        docs[i].user = docs[i].objectId.userId;
        docs[i].message = "Has shared a profile with you";

        switch (docs[i].objectId.sourceType) {
          case "user":
            docs[i].sourceUser = docs[i].objectId.sourceId;
            docs[i].sourceUser.videoId = docs[i].objectId.videoId;
            docs[i].videoType = "userVideo";
            break;
          case "campaign":
            docs[i].sourceUser = docs[i].objectId.sourceId.userId;
            docs[i].sourceUser.campId = docs[i].objectId.sourceId._id;
            docs[i].sourceUser.videoId = docs[i].objectId.videoId;
            docs[i].videoType = "campaignVideo";
            break;
          default:
            break;
        }
        break;
      default:
        break;
    }
    delete docs[i].objectId;
    delete docs[i].objectType;
  }
  return docs;
};

const notification = mongoose.model("notification", docSchema);

notification.once("index", (err) =>
  err ? logger.warn("Notification Models index error : ", err) : ""
);

module.exports = notification;
