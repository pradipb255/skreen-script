const mongoose = require("mongoose");
const logger = require("./../utils/logger");

const docSchema = new mongoose.Schema(
  {
    otp: { type: String, default: "", required: true },
    type: {
      type: String,
      enum: ["email"],
      default: "",
    },
    email: { type: String, default: "" },
  },
  { timestamps: true }
);

docSchema.index({ createdAt: 1 }, { expireAfterSeconds: 86400 }); // delete document after 1 day

const otp = mongoose.model("otp", docSchema);

otp.once("index", (err) =>
  err ? logger.warn("OTP Models index error : ", err) : ""
);

module.exports = otp;
