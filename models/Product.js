const mongoose = require("mongoose");

const docSchema = new mongoose.Schema(
  {
    name: {
      type: String,
    },
    itemId: {
      type: String,
    },
    priceType: {
      enum: ["renewable", "consumable"],
    },
    platform: {
      type: String,
      enum: ["android", "ios"],
    },
    paymentType: {
      type: String,
      enum: ["free", "paid"],
    },
    productInfo: {
      type: {
        type: String,
        enum: ["user", "campaign"],
      },
      userType: {
        type: String,
        enum: ["jobSeeker", "talentSeeker"], // job seeker, talent seeker
      },
    },
    metadata: {
      video: {
        limit: {
          type: Number,
          required: true,
        },
      },
      candidate: {
        limit: {
          type: Number,
          required: true,
        },
      },
      filterTalent: {
        type: Boolean,
      },
      saveTalent: {
        type: Number,
      },
      saveSearch: {
        type: Number,
      },
      alertNotification: {
        type: Boolean,
      },
    },
    isActive: {
      type: Boolean,
      default: true,
    },
  },
  { timestamps: true }
);

const product = mongoose.model("product", docSchema);

module.exports = product;
