const mongoose = require("mongoose");
const logger = require("../utils/logger");

const docSchema = new mongoose.Schema(
  {
    name: {
      type: String,
    },
    type: {
      type: String,
      default: "direct",
      enum: ["direct", "group"],
    },
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      required: true,
      index: true,
    },
  },
  { timestamps: true }
);

docSchema.indexes();

const room = mongoose.model("room", docSchema);

room.once("index", (err) =>
  err ? logger.warn("Room Models index error : ", err) : ""
);

module.exports = room;
