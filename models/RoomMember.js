const mongoose = require("mongoose");
const moment = require("moment");
const logger = require("../utils/logger");

const docSchema = new mongoose.Schema(
  {
    roomId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "room",
      index: true,
      required: true,
    },
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      index: true,
      required: true,
    },
    lastRead: {
      type: Date,
      default: moment().toDate(),
    },
  },
  { timestamps: true }
);

docSchema.index({ roomId: 1, userId: 1 }, { unique: true });

const roomMember = mongoose.model("roomMember", docSchema);

roomMember.once("index", (err) =>
  err ? logger.warn("Room member Models index error : ", err) : ""
);

module.exports = roomMember;
