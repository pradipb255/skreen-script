const mongoose = require("mongoose");
const logger = require("../utils/logger");

const docSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      index: true,
    },
    name: {
      type: String,
    },
    requestArgs: {
      type: String,
      get: function (value) {
        if (!isNaN(value)) return "";
        return JSON.parse(value);
      },
      set: (v) => JSON.stringify(v),
    },
    isLocked: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

docSchema.indexes();

/**
 * Apply getter and setter for object and json
 */
docSchema.set("toObject", { getters: true, virtuals: false });
docSchema.set("toJSON", { getters: true, virtuals: false });

const searchResult = mongoose.model("searchResult", docSchema);

searchResult.once("index", (err) =>
  err ? logger.warn("Search result Models index error : ", err) : ""
);

module.exports = searchResult;
