const mongoose = require("mongoose");
const logger = require("./../utils/logger");

const docSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Types.ObjectId,
      ref: "user",
    },
    refreshToken: {
      type: String,
    },
    token: {
      type: Number,
    },
    device: {
      type: String,
      enum: ["Android", "IOS"],
    },
    appVersion: {
      type: String,
    },
    deviceId: {
      type: String,
      index: true,
    },
    deviceToken: {
      type: String,
    },
    status: {
      type: String,
      enum: ["online", "offline"],
      index: true,
    },
    socketId: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

docSchema.index({ userId: 1 });

const session = mongoose.model("session", docSchema);

session.once("index", (err) =>
  err ? logger.warn("session Models index error : ", err) : ""
);

module.exports = session;
