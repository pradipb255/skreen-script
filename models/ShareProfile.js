const mongoose = require("mongoose");
const logger = require("../utils/logger");

const docSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
    videoId: {
      type: mongoose.Schema.Types.ObjectId,
      refPath: "videoType",
    },
    videoType: {
      type: String,
      enum: ["userVideo", "campaignVideo"],
    },
    sourceId: {
      type: mongoose.Schema.Types.ObjectId,
      refPath: "sourceType",
    },
    sourceType: {
      type: String,
      enum: ["user", "campaign"],
    },
    targetUserId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
  },
  { timestamps: true }
);

const shareProfile = mongoose.model("shareProfile", docSchema);

shareProfile.once("index", (err) =>
  err ? logger.warn("Share profile models index error : ", err) : ""
);

module.exports = shareProfile;
