const mongoose = require("mongoose");
const logger = require("../utils/logger");

const docSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      index: true,
    },
    submitterId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      index: true,
    },
    note: {
      type: String,
      default: "",
    },
    isRead: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

const submission = mongoose.model("submission", docSchema);

submission.once("index", (err) =>
  err ? logger.warn("Video like Models index error : ", err) : ""
);

module.exports = submission;
