const mongoose = require("mongoose");
const logger = require("../utils/logger");

let paymentType = {
  1: "ios",
  2: "android",
};

const docSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      refType: "user",
    },
    paymentType: {
      type: Number,
      enum: [1, 2],
      get: function (v) {
        return paymentType[v];
      },
      set: (v) =>
        Object.keys(paymentType).find((key) => paymentType[key] === v),
    },
    paymentId: {
      type: String,
      required: true,
    },
    referenceType: {
      type: String,
      enum: ["campaign", "userSubscription"],
    },
    referenceId: {
      type: mongoose.Schema.Types.ObjectId,
      refType: "referenceType",
    },
  },
  { timestamps: true }
);

docSchema.index({ paymentType: 1, paymentId: 1 }, { unique: true });
docSchema.indexes();

/**
 * Apply getter and setter for object and json
 */
docSchema.set("toObject", { getters: true, virtuals: false });
docSchema.set("toJSON", { getters: true, virtuals: false });

const transaction = mongoose.model("transaction", docSchema);

transaction.once("index", (err) =>
  err ? logger.warn("transaction Models index error : ", err) : ""
);

module.exports = transaction;
