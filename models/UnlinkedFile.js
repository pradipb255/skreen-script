const mongoose = require("mongoose");
const logger = require("../utils/logger");

const docSchema = new mongoose.Schema(
  {
    name: {
      type: String,
    },
    type: {
      type: String,
    },
  },
  { timestamps: true }
);

docSchema.index({ name: 1, type: 1 }, { unique: true });

docSchema.statics.link = function (data) {
  const createData = () =>
    new Promise((resolve, reject) => {
      this.create(data).then(resolve).catch(reject);
    });

  createData()
    .then()
    .catch((err) => {
      logger.error(`Link file error : ${err}`);
    });
};

docSchema.statics.unlink = function (name) {
  const removeData = () =>
    new Promise((resolve, reject) => {
      this.deleteOne({
        name,
      })
        .then(resolve)
        .catch(reject);
    });

  removeData()
    .then()
    .catch((err) => {
      logger.error(`Unlink file error : ${err}`);
    });
};

const UnlinkedFile = mongoose.model("UnlinkedFile", docSchema);

UnlinkedFile.once("index", (err) =>
  err ? logger.warn("UnlinkedFile Models index error : ", err) : ""
);

module.exports = UnlinkedFile;
