const mongoose = require("mongoose");
const bcrypt = require("bcrypt-nodejs");

let genderType = {
  M: "male",
  F: "female",
};

const docSchema = new mongoose.Schema(
  {
    businessName: {
      type: String,
      default: "",
    },
    firstName: {
      type: String,
      default: "",
    },
    lastName: {
      type: String,
      default: "",
    },
    email: {
      type: String,
      set: (v) => v.toLowerCase(),
      unique: true,
      index: true,
    },
    isEmailVerified: {
      type: Boolean,
      default: true,
    },
    profilePic: {
      type: String,
      default: "",
    },
    gender: {
      type: String,
      default: "",
      enum: ["male", "female", ""],
      set: (v) => {
        return genderType[v];
      },
    },
    loginType: {
      type: String,
      default: "normal",
      enum: ["normal", "google", "facebook", "apple"],
    },
    googleId: {
      type: String,
      index: true,
    },
    facebookId: {
      type: String,
      index: true,
    },
    appleId: {
      type: String,
      index: true,
    },
    password: {
      type: String,
    },
    userType: {
      type: String,
      enum: ["jobSeeker", "talentSeeker"], // job seeker, talent seeker
      default: "jobSeeker",
    },
    status: {
      type: String,
      default: "completed",
      enum: ["inComplete", "completed"],
    },
    isVerified: {
      type: Boolean,
      default: false,
    },
    setUpProfile: {
      type: Boolean,
      default: false,
    },
    location: {
      type: {
        type: String,
        enum: ["Point"],
      },
      coordinates: [
        {
          type: Number,
        },
      ],
    },
    address: {
      type: String,
    },
    about: {
      type: String,
      default: "",
    },
    radius: {
      type: Number,
      default: 0.0, // radius in mile
    },
    interest: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "interest",
      },
    ],
    socialPresence: {
      linkedin: {
        type: String,
        default: "",
      },
      twitter: {
        type: String,
        default: "",
      },
      facebook: {
        type: String,
        default: "",
      },
      instagram: {
        type: String,
        default: "",
      },
    },
    video: {
      limit: {
        type: Number,
      },
      // video count which is not locked and is enabled
      feedCount: {
        type: Number,
        default: 0,
      },
      availableDefault: {
        type: Boolean,
        default: false,
      },
    },
    features: {
      videoLimit: {
        type: Number,
      },
      filterTalent: {
        type: Boolean,
      },
      saveTalent: {
        type: Number,
      },
      saveSearch: {
        type: Number,
      },
      alertNotification: {
        type: Boolean,
      },
    },
    resetPasswordToken: {
      type: String,
    },
    resetPasswordExpires: {
      type: Date,
    },
    campaignCount: {
      type: Number,
    },
    subscriptionProductId: {
      type: String,
      default: "",
    },
  },
  {
    timestamps: true,
  }
);

/**
 * Password hash middleware.
 */
docSchema.pre("save", function save(next) {
  const user = this;

  if (!user.password || !user.isModified("password")) {
    return next();
  }

  // encrypt password
  bcrypt.genSalt(10, (err, salt) => {
    if (err) {
      return next(err);
    }
    bcrypt.hash(user.password, salt, null, (err, hash) => {
      if (err) {
        return next(err);
      }
      user.password = hash;
      next();
    });
  });
});

const user = mongoose.model("user", docSchema);

module.exports = user;
