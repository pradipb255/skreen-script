const mongoose = require("mongoose");
const logger = require("../utils/logger");

let receiptType = {
  1: "ios",
  2: "android",
};

const docSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      index: true,
    },
    productId: {
      type: String,
    },
    receipt: {
      type: String,
    },
    receiptType: {
      type: Number,
      enum: [1, 2],
      get: function (v) {
        return receiptType[v];
      },
      set: (v) =>
        Object.keys(receiptType).find((key) => receiptType[key] === v),
    },
    isExpired: {
      type: Boolean,
      default: false,
    },
    cancelledAt: {
      type: Date,
      default: null,
    },
    startDate: {
      type: Date,
    },
    endDate: {
      type: Date,
      index: true,
    },
  },
  {
    timestamps: true,
  }
);

docSchema.indexes();

const userSubscription = mongoose.model("userSubscription", docSchema);

userSubscription.once("index", (err) =>
  err ? logger.warn("userSubscriptions Models index error : ", err) : ""
);

module.exports = userSubscription;
