const mongoose = require("mongoose");
const logger = require("../utils/logger");

const docSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      index: true,
    },
    userVideoId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "userVideo",
      index: true,
    },
    message: {
      type: String,
    },
  },
  { timestamps: true }
);

docSchema.indexes();

const videoComment = mongoose.model("videoComment", docSchema);

videoComment.once("index", (err) =>
  err ? logger.warn("Video comment Models index error : ", err) : ""
);

module.exports = videoComment;
