const mongoose = require("mongoose");
const logger = require("../utils/logger");
const Notification = require("./Notification");

const docSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      index: true,
    },
    userVideoId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "userVideo",
      index: true,
    },
  },
  { timestamps: true }
);

docSchema.index({ userId: 1, userVideoId: 1 }, { unique: true });
docSchema.indexes();

// Only document middleware
docSchema.pre("remove", { document: true }, function (next) {
  Notification.deleteMany({
    activityType: "videoLike",
    objectId: this._id,
  })
    .then(() => next())
    .catch(next);
});

// Only query middleware. This will get called when you do `Model.remove()`
// but not `doc.remove()`.
docSchema.pre("remove", { query: true }, function (next) {
  Notification.deleteMany({
    activityType: "videoLike",
    objectId: this._id,
  })
    .then(() => next())
    .catch(next);
});

const videoLike = mongoose.model("videoLike", docSchema);

videoLike.once("index", (err) =>
  err ? logger.warn("Video like Models index error : ", err) : ""
);

module.exports = videoLike;
