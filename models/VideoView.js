const mongoose = require("mongoose");
const logger = require("../utils/logger");

const docSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      index: true,
    },
    userVideoId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "userVideo",
      index: true,
    },
    count: {
      type: Number,
      default: 1,
    },
  },
  { timestamps: true }
);

docSchema.index({ userId: 1, userVideoId: 1 }, { unique: true });
docSchema.indexes();

const videoView = mongoose.model("videoView", docSchema);

videoView.once("index", (err) =>
  err ? logger.warn("Video view Models index error : ", err) : ""
);

module.exports = videoView;
