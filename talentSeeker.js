const envVars = require("dotenv").config({
  path: "./.env",
});

const talentSeekerData = require("./data/talentSeeker.json");
const _ = require("lodash");
const AWS = require("./loaders/aws");
const request = require("request");
const url = require("url");
const moment = require("moment");
const sharp = require("sharp");
const https = require("https");
const fs = require("fs");
const path = require("path");
const crypto = require("crypto");
const ffmpeg = require("fluent-ffmpeg");
const ffprobe = require("ffprobe-static");
const FFMPEG = require("ffmpeg-static");
const s3bucket = new AWS.S3({ params: { Bucket: process.env.S3_BUCKET } });
// const NodeGeocoder = require("node-geocoder");

ffmpeg.setFfmpegPath(FFMPEG);
ffmpeg.setFfprobePath(ffprobe.path);

const userVideo = {
  limit: {
    jobSeeker: 1,
    talentSeeker: 1,
  },
};
const talentSeekerFeature = {
  filterTalent: false,
  saveTalent: 0,
  saveSearch: 0,
  alertNotification: false,
};

// load .env in local development
Object.keys(envVars).map((key) => {
  process.env[key] = envVars[key];
  return envVars[key];
});

const connectMongodb = require("./loaders/mongodb");
const models = require("./models");

connectMongodb(() => {
  let interests;

  const getInterest = () =>
    new Promise((resolve, reject) => {
      models.Interest.find()
        .lean()
        .then((data) => {
          interests = data;
          return resolve();
        })
        .catch(reject);
    });

  const getDownloadUrl = (urlStr) => {
    /**
     * get direct image
     */
    return `${urlStr}&raw=1`;
  };

  const getFileName = (str) => {
    return str.replace(/^.*[\\\/]/, "").split("?")[0];
  };

  const processData = ({ user, index }) =>
    new Promise((resolve, reject) => {
      console.log(`processing user no: ${index}`);

      let profilePicName, videoName, latitude, longitude, userInstance;

      let awsPromises = [];

      let aws = {
        profilePic: "",
        profileVideo: "",
        profileVideoThumb: "",
      };

      const uploadToS3 = () =>
        new Promise((resolve, reject) => {
          var params = {
            ACL: "public-read",
            Bucket: process.env.S3_BUCKET,
            Body: fs.createReadStream(path),
            Key: `${typeValidator[req.body.type].path}${name}`,
          };
          return s3bucket.upload(params).promise();
        });

      const fetchProfileImage = () =>
        new Promise((resolve, reject) => {
          profilePicName = getFileName(user["Profile Photo Link"]);

          const checkFileForLocal = () =>
            new Promise((resolve, reject) => {
              request.get(
                {
                  url: getDownloadUrl(user["Profile Photo Link"]),
                  encoding: null,
                },
                function (err, response, body) {
                  crypto.pseudoRandomBytes(16, (err, raw) => {
                    if (err) return reject(err);

                    let filename = `${raw.toString("hex")}${profilePicName}`;
                    aws.profilePic = filename;
                    // awsPromises.push(
                    s3bucket
                      .upload({
                        ACL: "public-read",
                        Bucket: process.env.S3_BUCKET,
                        Body: body,
                        Key: `Skreen/user/profile/${filename}`,
                      })
                      .promise()
                      .then(resolve)
                      .catch(reject);
                  });
                }
              );
            });

          checkFileForLocal()
            .then(resolve)
            .catch((err) => {
              console.error(err);
              console.log(`fetch profile Image error: index number: ${index}`);
              return reject(err);
            });
        });

      const fetchProfileVideo = () =>
        new Promise((resolve, reject) => {
          videoName = getFileName(user["Video Link(s)"]);
          let isExistsLocalFIle = false;
          let videoDest, videoThumbName;

          const checkFileForLocal = () =>
            new Promise((resolve, reject) => {
              crypto.pseudoRandomBytes(16, (err, raw) => {
                if (err) return reject(err);
                let url = getDownloadUrl(user["Video Link(s)"]);
                let filename = `${raw.toString("hex")}${videoName}`;
                aws.profileVideo = filename;
                videoDest = `./public/${filename}`;
                videoThumbName = `${filename.substr(
                  0,
                  filename.lastIndexOf(".")
                )}.jpg`;
                aws.profileVideoThumb = videoThumbName;
                let file = fs.createWriteStream(videoDest);

                request
                  .get({ url })
                  .pipe(file)
                  .on("finish", (data) => {
                    s3bucket
                      .upload({
                        ACL: "public-read",
                        Bucket: process.env.S3_BUCKET,
                        Body: fs.createReadStream(videoDest),
                        Key: `Skreen/user/video/${filename}`,
                      })
                      .promise()
                      .then(resolve)
                      .catch(reject);
                  })
                  .on("error", (err) => {
                    return reject(err);
                  });
              });
            });

          const createThumbnail = () =>
            new Promise((resolve, reject) => {
              // if (isExistsLocalFIle) return resolve();
              // return resolve();
              ffmpeg(path.join(videoDest))
                .thumbnail({
                  timestamps: ["0%"],
                  filename: videoThumbName,
                  folder: `./public`,
                  // size: '1280x720'
                })
                .on("end", () => {
                  s3bucket
                    .upload({
                      ACL: "public-read",
                      Bucket: process.env.S3_BUCKET,
                      Body: fs.createReadStream(
                        path.join("public", videoThumbName)
                      ),
                      Key: `Skreen/user/video/${videoThumbName}`,
                    })
                    .promise()
                    .then(resolve)
                    .catch(reject);
                })
                .on("error", (err) => {
                  return reject(err);
                });
            });

          checkFileForLocal()
            .then(createThumbnail)
            .then(resolve)
            .catch((err) => {
              console.error(err);
              console.log(`fetch profile video error: index number: ${index}`);
              return reject(err);
            });
        });

      const getLatLong = () =>
        new Promise((resolve, reject) => {
          request.get(
            {
              url: user["Google Map URL"],
              followRedirect: false,
            },
            function (err, response, body) {
              if (err) return reject(err);
              let regex = /(-?[\d]*\.[\d]*),(-?[\d]*\.[\d]*)/;
              let locationData = String(response.headers.location).match(regex);
              latitude = locationData[1];
              longitude = locationData[2];

              return resolve();
            }
          );
        });

      const createUserInstance = () =>
        new Promise((resolve, reject) => {
          let interestIndex = String(user["Looking for talent in"]).split(",");
          userInstance = new models.User({
            firstName: user["Contact First Name"],
            lastName: user["Contact Last Name"],
            businessName: user["Business Name"],
            profilePic: aws.profilePic,
            email: user["Contact Email"],
            password: user["Password"],
            userType: "talentSeeker",
            setUpProfile: true,
            isVerified: true,
            // gender: user["Gender"],
            address: `${user["City"]}, ${user["State"]}`,
            radius: user["Radius of Interest"],
            about: user["About Business"],
            location: {
              type: "Point",
              coordinates: [longitude, latitude],
            },
            interest: _.map(interestIndex, (i) => {
              return interests[Number(i) - 1]._id;
            }),
          });

          return resolve();
        });

      const findPlan = () =>
        new Promise((resolve, reject) => {
          let itemId;

          if (user["Pro User"]) {
            switch (user["Plan Frequency"]) {
              case "Monthly":
                itemId = "businessprousermonthly";
                break;
              case "Annual":
                itemId = "businessprouserannual";
                break;
              default:
                break;
            }
          } else if (user["Power User"]) {
            switch (user["Plan Frequency"]) {
              case "Monthly":
                itemId = "businesspoweruser";
                break;
              case "Annual":
                itemId = "businesspoweruserannual";
                break;
              default:
                break;
            }
          }

          models.Product.findOne({
            itemId,
          })
            .then((productDetail) => {
              let videoLimit = 1;
              if (itemId && productDetail) {
                videoLimit = productDetail.metadata.video.limit;
                userInstance.subscriptionProductId = itemId;
              }

              userInstance.video.limit = videoLimit;
              userInstance.features.videoLimit = videoLimit;
              userInstance.video.feedCount = 1;
              userInstance.video.availableDefault = true;

              /**
               * for talent seeker
               */
              userInstance.features.filterTalent =
                productDetail.metadata.filterTalent ||
                talentSeekerFeature.filterTalent;
              userInstance.features.saveTalent =
                productDetail.metadata.saveTalent ||
                talentSeekerFeature.saveTalent;
              userInstance.features.saveSearch =
                productDetail.metadata.saveSearch ||
                talentSeekerFeature.saveSearch;
              userInstance.features.alertNotification =
                productDetail.metadata.alertNotification ||
                talentSeekerFeature.alertNotification;

              return resolve();
            })
            .catch((err) => {
              console.error(err);
              console.log(`Find plan error: ${err}`);
              return reject(err);
            });
        });

      const saveToDb = () =>
        new Promise((resolve, reject) => {
          userInstance
            .save()
            .then((userData) => {
              models.UserVideo.create({
                userId: userData._id,
                video: aws.profileVideo,
                videoThumb: aws.profileVideoThumb,
                isDefault: true,
              })
                .then(() => {
                  console.debug(`created user no: ${index}`);
                })
                .catch(reject);
            })
            .catch((err) => {
              console.error(err);
              console.log(`save to db error no: ${index}`);
              return reject(err);
            });
        });

      fetchProfileImage()
        .then(fetchProfileVideo)
        .then(getLatLong)
        .then(createUserInstance)
        .then(findPlan)
        .then(saveToDb)
        .catch((err) => {
          console.error(err);
        });
    });

  const mapJobSeekerData = () =>
    new Promise(async (resolve, reject) => {
      let bulkData = [];
      for (const key in talentSeekerData) {
        const jobSeeker = talentSeekerData[key];

        processData({ user: jobSeeker, index: key });

        // let interestIndex = String(
        //   jobSeeker["Interested to find gig in"]
        // ).split(",");

        // let profilePicName = getFileName(jobSeeker["Profile Photo Link"]),
        //   videoName = getFileName(jobSeeker["Video Link(s)"]);

        // sharp(getDownloadUrl(jobSeeker["Profile Photo Link"]))
        //   .jpeg()
        //   .toBuffer()
        //   .then((bufferData) => {
        //     var params = {
        //       ACL: "public-read",
        //       Bucket: process.env.S3_BUCKET,
        //       Body: bufferData,
        //       // Key: `${profilePicName}`,
        //       Key: `${moment().unix()}`,
        //     };
        //     return s3bucket
        //       .upload(params)
        //       .promise()
        //       .then((data) => {
        //         console.log(data);
        //       })
        //       .catch((err) => {
        //         console.log(err);
        //       });
        //   })
        //   .catch(reject);

        // request.get(
        //   // getDownloadUrl(jobSeeker["Profile Photo Link"]),
        //   getDownloadUrl(jobSeeker["Video Link(s)"]),
        //   // function (
        //   // let regex = /(-?[\d]*\.[\d]*),(-?[\d]*\.[\d]*)/;
        //   // request.get(
        //   //   {
        //   //     url: "https://goo.gl/maps/KdPkKawdMpJnd8LF6",
        //   //     followRedirect: false,
        //   //   },
        //   function (err, response, body) {
        //     var params = {
        //       ACL: "public-read",
        //       Bucket: process.env.S3_BUCKET,
        //       Body: body,
        //       Key: `${profilePicName}`,
        //     };
        //     return s3bucket
        //       .upload(params)
        //       .promise()
        //       .then((data) => {
        //         console.log(data);
        //       })
        //       .catch((err) => {
        //         console.log(err);
        //       });
        //   }
        // );

        // var params = {
        //   ACL: "public-read",
        //   Bucket: process.env.S3_BUCKET,
        //   Body: fs.createReadStream(path),
        //   Key: `${typeValidator[req.body.type].path}${name}`,
        // };
        // return s3bucket.upload(params).promise();

        // bulkData.push({
        //   firstName: jobSeeker["First Name"],
        //   lastName: jobSeeker["Last Name"],
        //   email: jobSeeker["Email"],
        //   password: jobSeeker["Password"],
        //   gender: jobSeeker["Gender"],
        //   address: `${jobSeeker["City"]}, ${jobSeeker["State"]}`,
        //   radius: jobSeeker["Radius of Interest"],
        //   about: jobSeeker["About Me"],
        //   interest: _.map(interestIndex, (i) => {
        //     return interest[Number(i) - 1]._id;
        //   }),
        // });
      }

      // models.User.create(bulkData).then(resolve).catch(reject);
    });

  getInterest()
    .then(mapJobSeekerData)
    .then(() => console.log("success"))
    .catch((err) => console.log(err));
});
